# -*- coding: utf-8 -*-
"""
Created on Sat Mar 24 16:59:53 2018

@author: enovi
"""

import nltk

#Brand Personality
def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Brand Personality Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             analyze_text(article_text)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def analyze_text(article_text):
    #sincerity, excitement, competence, sophistication, ruggedness
    sinc_l = ['actual', 'airy', 'animated', 'artless', 'attested', 'authentic', 'bonafide',
              'breezy', 'bubbly', 'buoyant', 'candid', 'carefree', 'cheerful', 'content',
              'direct', 'ebullient', 'elated', 'exuberant', 'forward', 'frank', 'frolic',
              'genial', 'genuine', 'glad', 'glee', 'grin', 'guileless', 'happy', 'honest',
              'hope', 'ingenuous', 'jaunty', 'jolly', 'jovial', 'joyous', 'laugh', 'law',
              'legal', 'lighthearted', 'merry', 'mirth', 'natural', 'open', 'optimistic',
              'original', 'positive', 'pukka', 'radiant', 'real', 'right', 'sincere',
              'smile', 'sound', 'sparkling', 'sprightly', 'sterling', 'sunny', 'true',
              'truth', 'unadulterated', 'unaffected', 'unalloyed', 'undisputed', 'unfeigned',
              'unhypocritical', 'upbeat', 'upfront', 'valid', 'verified', 'veritable']
              
    exci_l = ['active', 'advanced', 'adventure', 'audacious', 'bold', 'brave', 'confident',
              'contemporary', 'cool', 'courage', 'creative', 'current', 'dare', 'dash',
              'dauntless', 'determined', 'dynamic', 'energetic', 'energy', 'enterprise',
              'enthusiastic', 'excite', 'fanciful', 'fashion', 'fearless', 'fiery', 'fire',
              'flash', 'fly', 'fresh', 'guts', 'happening', 'heedless', 'heroic', 'hip',
              'ingenious', 'innovate', 'inspire', 'intrepid', 'invent', 'latest', 'live',
              'madcap', 'mettle', 'mod', 'modern', 'natty', 'new', 'nifty', 'now', 'origin',
              'passion', 'pep', 'pluck', 'present', 'progress', 'push', 'recent', 'reckless',
              'resolute', 'resolve', 'resourceful', 'smart', 'snazzy', 'spiffy', 'sprightly',
              'spunk', 'swing', 'trend', 'unafraid', 'undaunted', 'unshrinking', 'unusual',
              'valiant', 'valorous', 'venture', 'vibrant', 'vigor', 'vision', 'vital',
              'vivacious', 'vogue', 'whimsical']
    
    comp_l = ['attest', 'authentic', 'business', 'certain', 'cohere', 'commit', 'conserve',
              'constant', 'dedicate', 'definitive', 'depend', 'devote', 'effect', 'efficient',
              'establish', 'faith', 'good', 'guard', 'hoard', 'honest', 'infallible', 'keep',
              'logic', 'loyal', 'maintain', 'method', 'order', 'perpetuate', 'plan', 'product',
              'prolong', 'protect', 'prove', 'reputable', 'reserve', 'responsible', 'safe',
              'save', 'secure', 'solid', 'sound', 'stable', 'staunch', 'stead', 'stock',
              'streamline', 'structure', 'sure', 'sustain', 'system', 'tested', 'tried', 'true',
              'trust', 'truth', 'unfailing', 'unswerving', 'unwavering', 'valid', 'well']
              
    soph_l = ['affair', 'allure', 'ardor', 'attach', 'attract', 'beauty', 'bright', 'brilliance',
              'brilliant', 'captivate', 'charisma', 'charm', 'chic', 'civilize', 'cosmopolitan',
              'cultivate', 'culture', 'dally', 'desire', 'elegant', 'enchant', 'enlighten',
              'excite', 'exotic', 'fascinate', 'flirt', 'fond', 'glitter', 'global', 'intimate',
              'intrigue', 'know', 'love', 'magic', 'magnet', 'mystery', 'mystique', 'passion',
              'polish', 'refine', 'romance', 'savvy', 'seduce', 'seductive', 'smooth',
              'sophisticate', 'spell', 'style', 'stylish', 'suave', 'thrill', 'urban', 'witch',
              'world']
    
    rugg_l = ['active', 'adapt', 'arduous', 'athletic', 'beefy', 'brawn', 'buff', 'buoyant',
              'burl', 'determine', 'durable', 'elastic', 'enduring', 'energetic', 'exacting',
              'firm', 'fit', 'flex', 'grit', 'hard', 'harsh', 'healthy', 'hearty', 'husky',
              'immune', 'impenetrable', 'imperishable', 'impervious', 'impregnable',
              'indestructible', 'inviolable', 'jacked', 'mesomorph', 'onerous', 'pliant',
              'power', 'proof', 'repel', 'resilient', 'resistant', 'resolute', 'rigor', 'ripped',
              'robust', 'rubber', 'shredded', 'solid', 'spartan', 'sport', 'spring', 'stalwart',
              'stark', 'stout', 'strain', 'strapping', 'strenuous', 'strong', 'sturdy',
              'substantial', 'supple', 'tenacious', 'tough', 'trim', 'unassailable',
              'unsusceptible', 'vigor']
    
    sinc_c = 0
    exci_c = 0
    comp_c = 0
    soph_c = 0
    rugg_c = 0
    traits = 0    
    
    article_text = article_text.lower().split()
    
    #stemming
    for word in article_text:
        word = nltk.PorterStemmer().stem(word)
    
    for word in sinc_l:
        word = nltk.PorterStemmer().stem(word)
    for word in exci_l:
        word = nltk.PorterStemmer().stem(word)
    for word in comp_l:
        word = nltk.PorterStemmer().stem(word)
    for word in soph_l:
        word = nltk.PorterStemmer().stem(word)
    for word in rugg_l:
        word = nltk.PorterStemmer().stem(word)
    
    #analysis
    for word in article_text:
        if word in sinc_l:
            sinc_c += 1
        elif word in exci_l:
            exci_c += 1
        elif word in comp_l:
            comp_c += 1
        elif word in soph_l:
            soph_c += 1
        elif word in rugg_l:
            rugg_c += 1
    
    traits = sinc_c + exci_c + comp_c + soph_c + rugg_c
    if traits == 0:
        print('No trait keywords were found to analyze.')
    else:
        print('Sincerity: ', sinc_c / traits)
        print('Excitement: ', exci_c / traits)
        print('Competence: ', comp_c / traits)
        print('Sophistication: ', soph_c / traits)
        print('Ruggedness: ', rugg_c / traits)

main()